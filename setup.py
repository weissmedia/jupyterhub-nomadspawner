from __future__ import print_function

import os
import sys

import toml
from setuptools import find_packages
from setuptools import setup

v = sys.version_info
if v[:2] < (3, 8):
    error = 'ERROR: jupyterhub-nomadspawner requires Python version 3.8 or above.'
    print(error, file=sys.stderr)
    sys.exit(1)

this = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(this, 'nomadspawner', '__version__.py')) as f:
    exec(f.read(), about)

project = toml.load('pyproject.toml', _dict=dict)
about['__version__'] = project['tool']['poetry']['version']

setup(
    name=about['__title__'],
    version=about['__version__'],
    install_requires=[
        'async_generator>=1.8',
        'escapism',
        'python-slugify',
        'jupyterhub>=1.4',
        'jinja2',
        'urllib3',
        'pyYAML',
        'python-nomad',
    ],
    python_requires='>=3.8',
    extras_require={'test': ['toml', 'flake8', 'pytest>=5.4', 'pytest-cov', 'pytest-asyncio>=0.11.0', 'pre-commit']},
    description=about['__description__'],
    url=about['__url__'],
    author=about['__author__'],
    author_email=about['__author_email__'],
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    license='???',
    packages=find_packages(),
    project_urls={
        'Documentation': '',
        'Source': about['__url__'],
    },
)
