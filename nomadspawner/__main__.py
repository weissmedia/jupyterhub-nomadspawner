#!/usr/bin/env python3
import argparse
import logging

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    parent_parser = argparse.ArgumentParser(add_help=False)
    parent_parser.add_argument(
        '--debug',
        default=False,
        required=False,
        action='store_true',
        dest='debug',
        help='debug flag',
    )

    main_parser = argparse.ArgumentParser()
    main_subparsers = main_parser.add_subparsers(title='Main command', dest='main_command')

    nomad_parser = main_subparsers.add_parser('nomad', help='Usage: nomad <command> [args]', parents=[parent_parser])

    nomad_command_subparsers = nomad_parser.add_subparsers(title='Common command', dest='nomad_common_command')

    nomad_run_parser = nomad_command_subparsers.add_parser(
        'run', help='Run a new job or update an existing job', parents=[parent_parser]
    )
    nomad_run_parser.add_argument(
        help='Starts running a new job or updates an existing job using the specification located at <path>. '
        'This is the main command used to interact with Nomad.',
        nargs='?',
        type=argparse.FileType('r'),
        dest='specs_path',
    )

    nomad_status_parser = nomad_command_subparsers.add_parser(
        'status',
        help='Display the status output for a resource',
        parents=[parent_parser],
    )
    nomad_status_parser.add_argument('status', choices=['A', 'B', 'C'])

    args = main_parser.parse_args()

    if 'specs_path' in args and args.specs_path:
        print(args.specs_path.read())

    print(args)
