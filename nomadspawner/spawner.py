"""
JupyterHub Spawner to spawn user notebooks on a Nomad cluster.

This module exports `NomadSpawner` class, which is the actual spawner
implementation that should be used by JupyterHub.
"""
import os
from enum import Enum
from textwrap import dedent
from concurrent.futures import ThreadPoolExecutor

import asyncio
import jupyterhub
import nomad
from jinja2.nativetypes import NativeEnvironment
from jupyterhub.spawner import Spawner
from traitlets import Any
from traitlets import Bool
from traitlets import default
from traitlets import Dict
from traitlets import Integer
from traitlets import Unicode

from .traitlets import MegaByteSpecification
from .traitlets import MegaHertzSpecification
from .utils import RecursiveNamespace
from .utils import timeout
from .volumenamingstrategy import escaped_format_volume_name

_jupyterhub_xy = "%i.%i" % (jupyterhub.version_info[:2])


class ClientStatus(Enum):
    PENDING = 'pending'
    RUNNING = 'running'
    COMPLETE = 'complete'
    FAILED = 'failed'
    LOST = 'lost'


class JobState(Enum):
    PENDING = 'pending'
    RUNNING = 'running'
    DEAD = 'dead'


class Allocation(RecursiveNamespace):
    @property
    def container_name(self):
        return f"{self.JobID}-{self.ID}"

    @property
    def port_discovery(self):
        return {j.Value for i in self.Resources.Networks for j in i.DynamicPorts if j.Label == 'DISCOVERY'}.pop()


class Evaluation(RecursiveNamespace):
    pass


class NomadSpawner(Spawner):

    timeout = Integer(
        30,
        config=True,
        help="""
            The timeout for single-user spawning.

            Defaults to `30`.            
            """,
    )

    image = Unicode(
        "jupyterhub/singleuser:%s" % _jupyterhub_xy,
        config=True,
        help="""The image to use for single-user servers.

        This image should have the same version of jupyterhub as
        the Hub itself installed.

        If the default command of the image does not launch
        jupyterhub-singleuser, set `c.Spawner.cmd` to
        launch jupyterhub-singleuser, e.g.

        Any of the jupyter docker-stacks should work without additional config,
        as long as the version of jupyterhub in the image is compatible.
        """,
    )

    memory = MegaByteSpecification(
        "300M",
        config=True,
        help="""
            Maximum number of bytes a single-user notebook server is allowed to use.

            Allows the following suffixes:
              - K -> Kilobytes
              - M -> Megabytes
              - G -> Gigabytes
              - T -> Terabytes

            If the single user server tries to allocate more memory than this,
            it will fail. There is no guarantee that the single-user notebook server
            will be able to allocate this much memory - only that it can not
            allocate more than this.

            **This is a configuration setting. Your spawner must implement support
            for the limit to work.** The default spawner, `LocalProcessSpawner`,
            does **not** implement this support. A custom spawner **must** add
            support for this setting for it to be enforced.
            """,
    )

    cpu = MegaHertzSpecification(
        "100M",
        config=True,
        help="""
            Maximum number of hertz a single-user notebook server is allowed to use.

            Allows the following suffixes:
              - K -> Kilohertz
              - M -> Megahertz
              - G -> Gigahertz
              - T -> Terahertz

            If the single user server tries to allocate more memory than this,
            it will fail. There is no guarantee that the single-user notebook server
            will be able to allocate this much memory - only that it can not
            allocate more than this.

            **This is a configuration setting. Your spawner must implement support
            for the limit to work.** The default spawner, `LocalProcessSpawner`,
            does **not** implement this support. A custom spawner **must** add
            support for this setting for it to be enforced.
            """,
    )

    datacenter = Unicode(
        "local",
        config=True,
        help="""
            """,
    )

    region = Unicode(
        "dev-local",
        config=True,
        help="""The image to use for single-user servers.

                This image should have the same version of jupyterhub as
                the Hub itself installed.

                If the default command of the image does not launch
                jupyterhub-singleuser, set `c.Spawner.cmd` to
                launch jupyterhub-singleuser, e.g.

                Any of the jupyter docker-stacks should work without additional config,
                as long as the version of jupyterhub in the image is compatible.
                """,
    )

    remove = Bool(
        True,
        config=True,
        help="""
                Specifies whether the job should be stopped and removed immediately (`remove=True`) or 
                deferred to the Nomad garbage collector (`remove=False`).
                """,
    )

    volumes = Dict(
        config=True,
        help=dedent(
            """
            Map from host file/directory to container (guest) file/directory
            mount point. 
            """
        ),
    )

    format_volume_name = Any(
        config=True,
        help="""Any callable that accepts a string template and a DockerSpawner instance as parameters in that order and returns a string.
            Reusable implementations should go in dockerspawner.VolumeNamingStrategy, tests should go in ...
            """,
    )

    job_spec_template = Unicode(
        """{
          "Job": {
          "Type": "service",
            "Datacenters": ["{{ datacenter }}"],
            "Region": "{{ region }}",            
            "ID": "jupyter-{{ user_name }}",
            "Namespace": "default",                        
            "TaskGroups": [
              {
                "Name": "jupyter",
                "Tasks": [
                    {
                        "Driver": "docker",
                        "Name": "jupyter-{{ user_name }}",
                        "Env": {{ env }},
                        "Config": {
                            "ports": ["DISCOVERY"],
                            "image": "{{ image }}",
                            "command": "start-notebook.sh",
                            "args": [
                                "--ip=0.0.0.0",
                                "--port=8888",
                                "--notebook-dir=/home/jovyan",
                                "--SingleUserNotebookApp.default_url=/lab",
                                "--SingleUserNotebookApp.ResourceUseDisplay.track_cpu_percent=True",
                                "--debug"
                            ],
                            "volumes": {{ volumes }}                                            
                        },                                                            
                        "Resources": {
                            "CPU": {{ cpu }},
                            "MemoryMB": {{ memory }}
                        }
                    }              
                ],
                "Networks": [
                  {
                    "DynamicPorts": [
                      {
                        "Label": "DISCOVERY",
                        "To": 8888
                      }
                    ]
                  }
                ]
              }
            ]            
          }
        }""",
        config=True,
        help="""
            Jinja2 Nomad Job Creation Template for User.
            """,
    )

    @property
    def escaped_name(self):
        return self.user.name

    @default("port")
    def _port_default(self):
        return 8888

    @default("format_volume_name")
    def _get_default_format_volume_name(self):
        return escaped_format_volume_name

    @property
    def mount_binds(self):
        """
        A different way of specifying nomad volumes using more advanced spec.
        Converts volumes dict to a list of nomad job -> group -> volume
        """

        def _fmt(v):
            return self.format_volume_name(v, self)

        mounts = []
        for host, container in self.volumes.items():
            mounts.append(f"{_fmt(host)}:{_fmt(container)}")
        return mounts

    _executor = None

    @property
    def executor(self):
        """single global executor"""
        cls = self.__class__
        if cls._executor is None:
            cls._executor = ThreadPoolExecutor(1)
        return cls._executor

    _client = None

    @property
    def client(self):
        """single global nomad client instance"""
        cls = self.__class__
        if cls._client is None:
            client = nomad.Nomad(host=os.getenv('NOMAD_HOST_ADDR'))
            cls._client = client
        return cls._client

    @property
    def job_id(self):
        return f'jupyter-{self.escaped_name}'

    async def waiter(self, func, obj, timout=None):
        with timeout(timout or self.timeout):
            while True:
                try:
                    o = await func(obj)
                    if o is not None:
                        break
                except Exception as e:
                    raise e
                await asyncio.sleep(0.3)
        self.log.debug("waiter object %s", o)
        return o

    async def register_job(self, job) -> Evaluation:
        response = await self.nomad('job', 'register_job', self.job_id, job)
        self.log.debug("register_job: %s", response)
        evaluation = Evaluation(** await self.waiter(self._get_evaluation, response['EvalID']))
        return evaluation

    async def _get_evaluation(self, nomad_eval_id) -> Evaluation:
        evaluation = await self.nomad('evaluation', 'get_evaluation', nomad_eval_id)
        self.log.debug("job_waiter: %s", evaluation)
        if evaluation['Status'] == 'complete':
            return evaluation

    _node_id = None

    async def get_allocation(self, nomad_eval_id) -> Allocation:
        allocations = await self.nomad('evaluation', 'get_allocations', nomad_eval_id)
        self.log.debug("evaluation.get_allocations(nomad_eval_id) length %s", len(allocations))
        for allocation in allocations:
            self.log.debug("allocation %s", allocation)
            alloc = Allocation(** await self.waiter(self._get_allocation, allocation, 120))
            self._node_id = allocation['NodeID']
            self.port = alloc.port_discovery
            return alloc

    async def _get_allocation(self, allocation):
        allocation = await self.nomad('allocation', 'get_allocation', allocation['ID'])
        self.log.debug("Allocation ClientStatus %s", allocation['ClientStatus'])
        state = ClientStatus(allocation['ClientStatus'])
        if state == ClientStatus.RUNNING:
            return allocation
        elif state in (ClientStatus.LOST, ClientStatus.FAILED):
            raise Exception()

    async def get_ip_and_port(self):
        """Queries Docker daemon for container's IP and port.

        If you are using network_mode=host, you will need to override
        this method as follows::

            def get_ip_and_port(self):
                return self.host_ip, self.port

        You will need to make sure host_ip and port
        are correct, which depends on the route to the container
        and the port it opens.
        """
        node = await self.nomad('node', 'get_node', self._node_id)
        self.log.debug('node %s', node['Attributes']['nomad.advertise.address'].split(':'))
        ip, _ = node['Attributes']['nomad.advertise.address'].split(':')
        return ip, self.port

    def get_env(self):
        """Return the environment dict to use for the Spawner.

        See also: jupyterhub.Spawner.get_env
        """

        env = super().get_env()
        del env['PATH']

        # deprecate image
        env['JUPYTER_IMAGE_SPEC'] = self.image
        env['JUPYTER_IMAGE'] = self.image

        return env

    @property
    def job_data(self):
        data = {
            'user_name': self.escaped_name,
            'env': self.get_env(),
            'image': self.image,
            'cpu': self.cpu,
            'memory': self.memory,
            'datacenter': self.datacenter,
            'region': self.region,
            'volumes': self.mount_binds,
        }
        self.log.debug("JOB_SPEC_DATA: %s", data)
        return data

    async def _render_job_spec(self):
        job_spec_template = NativeEnvironment().from_string(self.job_spec_template)
        job_spec = job_spec_template.render(self.job_data)
        self.log.debug("JOB_SPEC: %s", job_spec)
        return job_spec

    def load_state(self, state):
        """Restore state of spawner from database.

        Called for each user's spawner after the hub process restarts.

        `state` is a dict that'll contain the value returned by `get_state` of
        the spawner, or {} if the spawner hasn't persisted any state yet.

        Override in subclasses to restore any extra state that is needed to track
        the single-user server for that user. Subclasses should call super().
        """
        super().load_state(state)
        self.log.debug("load_state: %s", state)

    def _nomad(self, attribute, method, *args, **kwargs):
        """wrapper for calling docker methods

        to be passed to ThreadPoolExecutor
        """
        m = getattr(getattr(self.client, attribute), method)
        return m(*args, **kwargs)

    def nomad(self, attribute, method, *args, **kwargs):
        """Call a nomad method in a background thread

        returns a Future
        """
        return asyncio.wrap_future(
            self.executor.submit(self._nomad, attribute, method, *args, **kwargs)
        )

    async def start(self):
        """Start the single-user server as a nomad job.

        If the job exists and `c.NomadSpawner.remove` is true, then
        the job is removed first. Otherwise, the existing jobs
        will be restarted.
        """

        self.log.debug("Job ID: %s", self.job_id)

        job = await self._render_job_spec()
        self.log.debug("Job: %s Type: %s", job, type(job))

        evaluation = await self.register_job(job)
        self.log.debug("evaluation %s", evaluation)

        allocation = await self.get_allocation(evaluation.ID)
        self.log.debug("port_discovery %s", allocation.port_discovery)

        ip, port = await self.get_ip_and_port()
        self.log.debug("ip %s port %s", ip, port)
        return ip, port

    async def stop(self, now=False):
        """Stop the job

        Will remove the job if `c.NomadSpawner.remove` is `True`.
        """
        self.log.info("Stopping (id: %s)", self.job_id)
        await self.nomad('job', 'deregister_job', self.job_id)

        if self.remove:
            await self.nomad('job', 'deregister_job', self.job_id, True)

        self.clear_state()

    async def poll(self):
        """Check for the job id"""

        job = await self.nomad('job', 'get_job', self.job_id)

        self.log.debug("Job %s status: %s", self.job_id, job['Status'])

        if JobState(job['Status']) == JobState.RUNNING:
            return None

        else:
            return (
                "ExitCode={ExitCode}, "
                "Error='{Error}', "
                "FinishedAt={FinishedAt}".format(ExitCode='', Error=job['Status'], FinishedAt='')
            )
