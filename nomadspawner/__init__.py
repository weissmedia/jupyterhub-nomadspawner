"""
JupyterHub Spawner to spawn user notebooks on a Nomad cluster.
After installation, you can enable it by adding::
    c.JupyterHub.spawner_class = 'nomadspawner.NomadSpawner'
in your `jupyterhub_config.py` file.
"""
# We export NomadSpawner specifically here. This simplifies import for users.
# Users can simply import nomadspawner.NomadSpawner in their applications
# instead of the more verbose import nomadspawner.spawner.NomadSpawner.
from nomadspawner.spawner import NomadSpawner

__all__ = ['NomadSpawner']
