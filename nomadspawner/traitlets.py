from jupyterhub.traitlets import ByteSpecification
from jupyterhub.traitlets import Integer
from jupyterhub.traitlets import TraitError


class MegaByteSpecification(ByteSpecification):
    """
    Allow easily specifying bytes in units of 1024 with suffixes

    Suffixes allowed are:
      - K -> Kilobyte
      - M -> Megabyte
      - G -> Gigabyte
      - T -> Terabyte
    """

    UNIT_SUFFIXES = {
        'K': 1024,
        'M': 1024 ** 2,
        'G': 1024 ** 3,
        'T': 1024 ** 4,
    }

    # Default to allowing None as a value
    allow_none = True

    def validate(self, obj, value):
        """
        Validate that the passed in value is a valid memory specification

        It could either be a pure int, when it is taken as a byte value.
        If it has one of the suffixes, it is converted into the appropriate
        pure byte value.
        """
        if isinstance(value, (int, float)):
            return int(value)

        try:
            num = float(value[:-1])
        except ValueError:
            raise TraitError(
                '{val} is not a valid memory specification. Must be an int or a string with suffix K, M, G, T'.format(
                    val=value
                )
            )
        suffix = value[-1]
        if suffix not in self.UNIT_SUFFIXES:
            raise TraitError(
                '{val} is not a valid memory specification. Must be an int or a string with suffix K, M, G, T'.format(
                    val=value
                )
            )
        else:
            return int((float(num) * self.UNIT_SUFFIXES[suffix]) / self.UNIT_SUFFIXES['M'])


class MegaHertzSpecification(Integer):
    """
    Allow easily specifying hertz in units of 1000 with suffixes

    Suffixes allowed are:
      - K -> Kilohertz
      - M -> Megahertz
      - G -> Gigahertz
      - T -> Terahertz
    """

    UNIT_SUFFIXES = {
        'K': 1000,
        'M': 1000 ** 2,
        'G': 1000 ** 3,
        'T': 1000 ** 4,
    }

    # Default to allowing None as a value
    allow_none = True

    def validate(self, obj, value):
        """
        Validate that the passed in value is a valid memory specification

        It could either be a pure int, when it is taken as a byte value.
        If it has one of the suffixes, it is converted into the appropriate
        pure byte value.
        """
        if isinstance(value, (int, float)):
            return int(value)

        try:
            num = float(value[:-1])
        except ValueError:
            raise TraitError(
                '{val} is not a valid memory specification. Must be an int or a string with suffix K, M, G, T'.format(
                    val=value
                )
            )
        suffix = value[-1]
        if suffix not in self.UNIT_SUFFIXES:
            raise TraitError(
                '{val} is not a valid memory specification. Must be an int or a string with suffix K, M, G, T'.format(
                    val=value
                )
            )
        else:
            return int(float(num) * self.UNIT_SUFFIXES[suffix] / self.UNIT_SUFFIXES['M'])
