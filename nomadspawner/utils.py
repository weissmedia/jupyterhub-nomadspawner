import signal
from contextlib import contextmanager
from types import SimpleNamespace


@contextmanager
def timeout(time):
    def raise_timeout(*args):
        raise TimeoutError

    # Register a function to raise a TimeoutError on the signal.
    signal.signal(signal.SIGALRM, raise_timeout)
    # Schedule the signal to be sent after ``time``.
    signal.alarm(time)

    try:
        yield
    except TimeoutError:
        pass
    finally:
        # Unregister the signal so it won't be triggered
        # if the timeout is not reached.
        signal.signal(signal.SIGALRM, signal.SIG_IGN)


class RecursiveNamespace(SimpleNamespace):

    def __init__(self, /, **kwargs):
        super(RecursiveNamespace, self).__init__()
        self.__dict__.update({k: self.__self(v) for k, v in kwargs.items()})

    def __self(self, obj):
        if isinstance(obj, dict):
            return type(self)(**obj)
        if isinstance(obj, (list, tuple)):
            return [self.__self(i) for i in obj]
        return obj

