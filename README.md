# NomadSpawner

The _jupyterhub_nomadspawner_, enables
[JupyterHub](https://github.com/jupyterhub/jupyterhub) to spawn single user
notebook servers on a [Nomad](https://www.nomadproject.io/) cluster.

NomadSpawner: takes an authenticated user and spawns a notebook server
in a Docker container for the user.

See the [NomadSpawner documentation]() for more information about features and usage.

## Prerequisites

JupyterHub 1.4 or above is required, which also means Python 3.8 or above.

## Features

Coming soon....

## Installation

Install nomadspawner to the system:

```bash
pip install nomadspawner
```

## License

All code is licensed under the terms of the revised BSD license.

## Resources

### JupyterHub

- JupyterHub tutorial | [Repo](https://github.com/jupyterhub/jupyterhub-tutorial)
  | [Tutorial documentation](http://jupyterhub-tutorial.readthedocs.io/en/latest/)
- [Documentation for JupyterHub](http://jupyterhub.readthedocs.io/en/latest/) | [PDF (latest)](https://media.readthedocs.org/pdf/jupyterhub/latest/jupyterhub.pdf) | [PDF (stable)](https://media.readthedocs.org/pdf/jupyterhub/stable/jupyterhub.pdf)
- [Documentation for JupyterHub's REST API](http://petstore.swagger.io/?url=https://raw.githubusercontent.com/jupyter/jupyterhub/HEAD/docs/rest-api.yml#/default)

### Project Jupyter

- [Documentation for Project Jupyter](http://jupyter.readthedocs.io/en/latest/index.html) | [PDF](https://media.readthedocs.org/pdf/jupyter/latest/jupyter.pdf)
- [Project Jupyter website](https://jupyter.org)
